﻿using UnityEngine;
using System.Collections;
using Prime31.StateKit;

public class App : MonoBehaviour
{
  static App _instance = null;
  public static SKStateMachine<MenuModel> _AppState;
  private App()
  {

  }
  public static App instance
  {
    get
    {
      if (_instance == null)
      {
        _instance = GameObject.FindObjectOfType<App>();
        //Tell unity not to destroy this object when loading a new scene!
        DontDestroyOnLoad(_instance.gameObject);
      }
      return _instance;
    }
  }

  void Awake()
  {
    if (_instance == null)
    {
      //If I am the first instance, make me the Singleton
      _instance = this;
      MenuModel menu = new MenuModel();
      _AppState = new SKStateMachine<MenuModel>(menu, new MainMenuState());
      _AppState.addState(new GameState());
      DontDestroyOnLoad(this);
    }
    else
    {
      //If a Singleton already exists and you find
      //another reference in scene, destroy it!
      if (this != _instance)
        Destroy(this.gameObject);
    }
  }

  public void changeState(string state)
  {
    if (state == "GameState")
      _AppState.changeState<GameState>();
    else if (state == "MainMenuState")
      _AppState.changeState<MainMenuState>();
    else
      Debug.Log(" Invalid state to transit " + state);
  }

  // Update is called once per frame
  void Update()
  {
    _AppState.update(Time.deltaTime);
  }
}