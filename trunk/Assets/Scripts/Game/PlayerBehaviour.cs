﻿using UnityEngine;
using System.Collections;


public class PlayerBehaviour : MonoBehaviour
{

  public enum PlayerState
  {
    Normal,
    Eaten,
    Scaled,
  };
  public float force = 100.0f;
  public PlayerState m_playerState = PlayerState.Normal;
  public int size;
  // Use this for initialization
  void Start()
  {
    SetState(PlayerState.Normal);
  }

  void SetSize(int amt)
  {
    size = amt;
    SpriteRenderer sr = this.GetComponentInParent<SpriteRenderer>();
    sr.transform.localScale = Util.BasePlayerScale * size;
    Util.PlayerSize = size;
//    LeanTween.scale(this.gameObject, Vector3.one, 1.0f).setEase(LeanTweenType.linear);
  }
  // Update is called once per frame
  void Update()
  {
    if (Input.GetMouseButton(0))
      rigidbody2D.AddForce(new Vector2(0, rigidbody2D.mass * force));
  }

  public void SetState(PlayerState state)
  {
    m_playerState = state;
    switch (m_playerState)
    {
      case PlayerState.Normal:
        {
          SetSize(2);
          break;
        }
      case PlayerState.Scaled:
        {
          SetSize(4);
          break;
        }
      case PlayerState.Eaten:
        {
          SetSize(1);
          break;
        }
    }
  }
}
