﻿using UnityEngine;
using System.Collections;

public class Fish : MonoBehaviour
{
  public AudioClip collideSound;
  public int size;
  GameManager gameManager = null;
  void Start()
  {
  }
  void TryInitGameManager()
  {
    if (GameObject.FindGameObjectWithTag("Manager") != null)
      gameManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<GameManager>() as GameManager;
  }
  void OnEnable()
  {
    if (gameManager == null)
      TryInitGameManager();
    if (gameManager == null)
    {
      OnDisable();
      return;
    }
    SetSize();
    if (gameObject.GetComponent<PolygonCollider2D>() != null)
      Destroy(gameObject.GetComponent<PolygonCollider2D>());
    gameObject.AddComponent<PolygonCollider2D>().isTrigger = true;
    gameObject.SetActive(true);
  }
  void SetSize()
  {
    SpriteRenderer sr = this.GetComponentInParent<SpriteRenderer>();
    sr.sprite = gameManager.GetRandomFish();
    size = Random.Range(1, Util.MaxFishSize);
    sr.transform.root.localScale = size * Util.BaseFishScale;
  }
  public float GetSize()
  {
    return size;
  }
  void OnDisable()
  {
    gameObject.SetActive(false);
  }
  public int Score()
  {
    return size * (int)Util.GameConstants.FishBaseScore;
  }
  void OnTriggerEnter2D(Collider2D collider)
  {
    if (collider.tag == "Player")
    {
      Util.LogInfo("Fish Size is " + size.ToString());
      if (Util.PlayerSize < size)
      {
        GameManager.instance.EatenByFish();
      }
      else
      {
        Tracking.EatFish(1, Score());
        OnDisable();
      }
      AudioSource.PlayClipAtPoint(collideSound, transform.position);
    }
    else if (collider.tag == "Wall")
    {
      Tracking.IncMissedCoins(1);
      OnDisable();
    }
  }
}
