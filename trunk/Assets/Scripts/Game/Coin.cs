﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{
  public AudioClip collideSound;
  void OnEnable()
  {
    gameObject.SetActive(true);
  }

  void OnDisable()
  {
    gameObject.SetActive(false);
  }
  void OnTriggerEnter2D(Collider2D collider)
  {
    if (collider.tag == "Player")
    {
      Tracking.EarnCoins(1);
      AudioSource.PlayClipAtPoint(collideSound, transform.position);
      OnDisable();

    }
    else if (collider.tag == "Wall")
    {
      Tracking.IncMissedCoins(1);
      OnDisable();
    }
  }
}
