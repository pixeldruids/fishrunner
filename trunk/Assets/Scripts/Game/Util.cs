﻿using UnityEngine;
using System.Collections;

public class Util : MonoBehaviour
{

  public static float boundary_x;
  public static int MaxFishSize = 5;
  public static Vector3 BaseFishScale = new Vector3(0.5f, 0.5f, 1.0f);
  public static Vector3 BasePlayerScale = new Vector3(0.0125f, 0.025f, 1.0f);

  public static int PlayerSize = 0;
  public Transform wall;
  public enum GameConstants
  {
    FishBaseScore = 30
  };
  // Use this for initialization
  void Start()
  {
    boundary_x = wall.collider2D.bounds.max.x;
  }
  public static void LogError(string msg)
  {
    Log(msg, Color.red);
  }
  public static void LogInfo(string msg)
  {
    Log(msg, Color.black);
  }
  static void Log(string msg, Color color)
  {
    TextMesh debugText = GameObject.FindGameObjectWithTag("debugText").GetComponent<TextMesh>() as TextMesh;
    if (debugText != null)
    {
      debugText.text = msg;
      debugText.color = color;
    }
  }
}
