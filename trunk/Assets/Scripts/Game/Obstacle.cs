﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour
{

  public float time = 5.0f;

  public enum Type
  {
    CoinBox,
    Laser,
    FishGroup
  }
  [System.Flags]
  public enum Behaviour
  {
    PingPongY = 1 << 0, // 1
  }
  public Type type;
  void OnEnable()
  {
    LeanTween.moveX(gameObject, Util.boundary_x - 2.0f, time).setOnComplete(() => { gameObject.Recycle(); });
    SetType(type);
  }
  void SetType(Type ty)
  {
    type = ty;
    switch (type)
    {
      case Type.CoinBox:
        LeanTween.moveY(gameObject, gameObject.transform.position.y + 3, 2.0f).setLoopPingPong();
        break;
      case Type.FishGroup:
        LeanTween.moveY(gameObject, gameObject.transform.position.y + 3, 2.0f).setLoopPingPong();
        break;
    }

  }
}
