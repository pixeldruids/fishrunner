using UnityEngine;
using System.Collections;

public class GravityWell : MonoBehaviour
{
  public int sign = -1;
  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }
  void OnTriggerStay2D(Collider2D hit)
  {
    GameObject obj = hit.gameObject;
    if (obj.rigidbody2D != null)
    {
      Vector2 pos = new Vector2(transform.position.x, transform.position.y);
      Vector2 tar_pos = new Vector2(obj.transform.position.x, obj.transform.position.y);
      obj.rigidbody2D.velocity += (pos - tar_pos).normalized * 300.0f * Time.deltaTime * sign;
    }
  }
}
