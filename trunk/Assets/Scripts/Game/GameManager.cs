﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
  public float scrollSpeed = 0.05f;
  public Obstacle[] ObstacleList;
  public Sprite[] FishSpriteList;

  public Transform refPoint;
  public PlayerBehaviour player;

  //We make a static variable to our GameManager instance
  public static GameManager instance { get; private set; }

  //When the object awakens, we assign the static variable
  void Awake()
  {
    instance = this;
  }

  void OnEnable()
  {
    InvokeRepeating("GenerateObstacle", 1.0f, 2.0f);
  }
  void FixedUpdate()
  {
    Vector2 offset = renderer.material.GetTextureOffset("_MainTex");
    offset.x += scrollSpeed * Time.deltaTime;
    offset.x %= 1;
    renderer.material.SetTextureOffset("_MainTex", offset);
  }

  void GenerateObstacle()
  {
    int index = UnityEngine.Random.Range(0, ObstacleList.Length);
    ObstacleList[index].Spawn(GetRandomGenPoint());
  }

  Vector3 GetRandomGenPoint()
  {
    Bounds bounds = refPoint.renderer.bounds;
    float y = UnityEngine.Random.Range(bounds.min.y, bounds.max.y);
    return new Vector3(bounds.min.x, y, 0);
  }

  public Sprite GetRandomFish()
  {
    return FishSpriteList[UnityEngine.Random.Range(0, FishSpriteList.Length)];
  }

  public void EatenByFish()
  {
    player.SetState(PlayerBehaviour.PlayerState.Eaten);
    Invoke("GameOver", 0.2f);
  }
  public void GameOver()
  {
    CancelInvoke("GenerateObstacle");
    App.instance.changeState("MainMenuState");
  }
}
