﻿using UnityEngine;
using System.Collections;

public class Tracking : MonoBehaviour
{
  static int currentCoins;
  static int missedCoins;
  static int fishCount;
  static int fishScore;
  public TextMesh FishScore;
  public TextMesh CoinScore;
  // Use this for initialization
  void Start()
  {
    currentCoins = 0;
    missedCoins = 0;
    fishScore = 0;
  }

  public static void EarnCoins(int amount)
  {
    currentCoins += amount;
  }

  public static void IncMissedCoins(int amount)
  {
    missedCoins += amount;
  }

  public static void EatFish(int amount, int score)
  {
    fishCount += amount;
    fishScore += score;
  }

  void FixedUpdate()
  {
    FishScore.text = fishScore.ToString();
    CoinScore.text = currentCoins.ToString();
  }

}
