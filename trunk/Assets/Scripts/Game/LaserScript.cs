﻿using UnityEngine;
using System.Collections;

public class LaserScript : MonoBehaviour
{
  public Sprite laserOnSprite;
  public Sprite laserOffSprite;
  public float interval = 0.5f;
  public float rotationSpeed = 0.0f;
  private bool isLaserOn = true;
  private bool isBlinkingLaser = true;
  private float timeUntilNextToggle;

  // Use this for initialization
  void Start()
  {
    timeUntilNextToggle = interval;
    if (Random.Range(0, 100) < 50)
    {
      isBlinkingLaser = false;
      isLaserOn = true;
    }
  }

  // Update is called once per frame
  void Update()
  {

  }

  void FixedUpdate()
  {
    timeUntilNextToggle -= Time.fixedDeltaTime;
    if (timeUntilNextToggle <= 0)
    {
      if (isBlinkingLaser)
        isLaserOn = !isLaserOn;
      collider2D.enabled = isLaserOn;
      SpriteRenderer spriteRenderer = ((SpriteRenderer)this.renderer);
      if (isLaserOn)
        spriteRenderer.sprite = laserOnSprite;
      else
        spriteRenderer.sprite = laserOffSprite;
      timeUntilNextToggle = interval;
    }
    transform.RotateAround(transform.position,
                           Vector3.forward,
                           rotationSpeed * Time.fixedDeltaTime);
  }

  void OnEnable()
  {
    gameObject.SetActive(true);
  }

  void OnDisable()
  {
    gameObject.SetActive(false);
  }

  void OnTriggerEnter2D(Collider2D collider)
  {
    if (collider.tag == "Player")
    {
      if (isLaserOn)
        DoGameOver();
    }
    else if (collider.tag == "Wall")
    {
      OnDisable();
    }
  }
  void OnTriggerStay2D(Collider2D collider)
  {
    if (collider.tag == "Player")
    {
      if (isLaserOn)
        DoGameOver();
    }
  }

  void DoGameOver()
  {
    GameManager gm = GameObject.FindGameObjectWithTag("Manager").GetComponent<GameManager>() as GameManager;
    gm.GameOver();
  }
}
