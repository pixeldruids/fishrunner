﻿using UnityEngine;
using System.Collections;
using Prime31.StateKit;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameState : SKState<MenuModel>
{
  public override void begin()
  {
    base.begin();
    Application.LoadLevel("Game");
  }
  // Update is called once per frame
  public override void update(float deltaTime)
  {

  }

  public override void end()
  {
    base.end();
  }
}

