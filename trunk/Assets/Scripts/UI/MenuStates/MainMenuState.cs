﻿using UnityEngine;
using System.Collections;
using Prime31.StateKit;
using UnityEngine.UI;


public class MainMenuState : SKState<MenuModel>
{

  public override void begin()
  {
    base.begin();
    Application.LoadLevel("Menu");
  }

  // Update is called once per frame
  public override void update(float deltaTime)
  {

  }

  public override void end()
  {
    base.end();
  }
}
