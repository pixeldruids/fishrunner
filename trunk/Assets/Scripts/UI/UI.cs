﻿using UnityEngine;
using System.Collections;
using Prime31.StateKit;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UI : MonoBehaviour
{
  public Button playButton;

  void Start()
  {
    InitMainMenu();
  }
  public void InitMainMenu()
  {
    playButton = GameObject.FindWithTag("ui_play_button").GetComponent<Button>() as Button;
    if (playButton != null)
      playButton.onClick.AddListener(() => App.instance.changeState("GameState"));
  }
}
